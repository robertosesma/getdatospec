package com.leam.getdatafrompec;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDCheckBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDComboBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

import com.stata.sfi.Data;
import com.stata.sfi.Frame;
import com.stata.sfi.Missing;
import com.stata.sfi.SFIToolkit;


public class ExtractMethods {

	public static void getDatos(String dir) throws IOException {
        // get all the pdf files of dir
        File folder = new File(dir);
        FilenameFilter pdfFilter;
        pdfFilter = (File dir1, String name) -> { return name.toLowerCase().endsWith(".pdf"); };
        File[] PECs = folder.listFiles(pdfFilter);

        boolean lcoment = false;
        boolean lhonor = false;
        boolean lape1 = false;
        boolean lape2 = false;
        boolean lnom = false;
        List<String> lines = new ArrayList<>();
        List<String> comments = new ArrayList<>();
        List<String> problems = new ArrayList<>();
        List<String> mlines = new ArrayList<>();
        
        for (File file : PECs) {
            if (file.isFile()) {
            	// get dni from filename
                String n = file.getName();
                String dni = n.substring(n.lastIndexOf("_")+1,n.lastIndexOf("."));
                
                System.out.println(dni);

		        // open pdf form
				PDDocument pdf = PDDocument.load(file);
			    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
			    String producer = pdf.getDocumentInformation().getProducer();		// get form producer                
                if (form.getFields().size()>0) {
                    if (!producer.substring(0,Math.min(11,producer.length())).equalsIgnoreCase("LibreOffice")) {
                    	// if the producer is not LibreOffice, the PDF file may be corrupted
                        problems.add(dni + "; " + producer);
                    }
                    
                	List<ControlPos> controls = getControls(pdf);
                	// detect APE1, APE2, NOMBRE, HONOR, COMENT fields
                	Iterator<PDField> it = form.getFieldIterator();
			    	while (it.hasNext()) {
			    		PDField f = it.next();
			    		if (!f.isReadOnly()) {
        					String name = f.getFullyQualifiedName();
        					if (name.equalsIgnoreCase("APE1")) lape1 = true;						// there's APE1 field
        					if (name.equalsIgnoreCase("APE2")) lape2 = true;						// there's APE2 field
        					if (name.equalsIgnoreCase("NOMBRE")) lnom = true;						// there's NOMBRE field
        					if (name.equalsIgnoreCase("HONOR")) lhonor = true;						// there's HONOR field
        					if (name.equalsIgnoreCase("COMENT")) lcoment = true;					// there's COMENT field
			    		}
			    	}
					
                    // build COMMENTS section
                    if (lcoment) {
	                    if (!form.getField("COMENT").getValueAsString().isEmpty()) {
	                        comments.add(dni + ":" + form.getField("COMENT").getValueAsString());
	                    }
                    }
					
					// build line data for current file
                    // header with identification data
                    String c = (lape1 ? "'" + form.getField("APE1").getValueAsString() + "'" : "null") + "," +
                    		(lape2 ? "'" + form.getField("APE2").getValueAsString() + "'" : "null") + "," +
                    		(lnom ? "'" + form.getField("NOMBRE").getValueAsString() + "'" : "null") + "," +
                    		"'" + dni + "'";
                    if (lhonor) {
                    	PDCheckBox honor = (PDCheckBox) form.getField("HONOR");
                        c = c + (honor.isChecked() ? ",1" : ",0");
                    }
                    // loop through the sorted fields and get the contents
					for (ControlPos control : controls) {
						if (!control.isW()) c = c + ",'" + control.getValue() + "'";
					}
                    lines.add(c);
                    
                    // loop through the sorted memos (if any) and get the contents
                    List<ControlPos> memos = getControlsMulti(pdf);
                    if (!memos.isEmpty()) {
                    	String m = "'" + dni + "'";	                    	
						for (ControlPos control : memos) {
							m = m + ",'" + control.getValue() + "'";
						}
	                    mlines.add(m);
                	}
                } else {
                	// if there are no fields on the form the PDF file may be corrupted
                    if (form.getFields().isEmpty()) {
                        problems.add(dni + "; no fields");
                    }
                }

				// close pdf form
		        pdf.close();
		        pdf = null;
		        form = null;
            }
        }
        // save data
        Files.write(Paths.get(dir + "/datos_pecs.txt"), lines, Charset.forName("UTF-8"));
        // save comments, if any
        if (!comments.isEmpty()) Files.write(Paths.get(dir + "/comentarios.txt"), comments, Charset.forName("UTF-8"));
        // save problems, if any
        if (!problems.isEmpty()) Files.write(Paths.get(dir + "/errores.txt"), problems, Charset.forName("UTF-8"));
        // save memos, if any
        if (!mlines.isEmpty()) Files.write(Paths.get(dir + "/memos.txt"), mlines, Charset.forName("UTF-8"));

        JOptionPane.showMessageDialog(null, "Procés completat." +
                (!comments.isEmpty() ? " Hi ha comentaris." : "") + 
                (!problems.isEmpty() ? " Hi ha errors." : ""));
	}

	public static void getSol(String dir) throws IOException {
    	List<ControlPos> controls = getControlsWithNames(dir);
    	List<String> lines = new ArrayList<>();
        // build sol txt
        int i = 0;
    	lines.add("id,eti,tipres,rescor,val,numopc");
        for (ControlPos c : controls) {
        	if (!c.isW()) {
        		i = i+1;
        		lines.add( String.valueOf(i) + ",'" + c.getName() + "'," + 	// id, eti (name)
        					String.valueOf(c.getTipo()) + ",''," +			// tipres (answer type), rescor (by def "")
        					c.getWAsString() + "," + c.getNumOpc());		// val (weight), numopc (null if not list) 		
        	}
        }
        // save data to file
        Files.write(Paths.get(dir.replace(".pdf","_sol.txt")), lines, Charset.forName("UTF-8"));

        JOptionPane.showMessageDialog(null, "Procés completat.");
	}

	public static int getNota(String args[]) {
		String pec = args[0];
		long obs = Long.parseLong(args[1]);
		int rc = 0;
		
		try {
	        // open form PEC and get controls
			File file = new File(pec);
			PDDocument pdf = PDDocument.load(file);
			List<ControlPos> controls = getControls(pdf);
	        pdf.close();
	        pdf = null;
		
		    // get sol data frame from Stata
		    Frame sol = Frame.connect("sol");
			int iP = sol.getVarIndex("p"); 
			int iW = sol.getVarIndex("w"); 
			int iT = sol.getVarIndex("tipo");
			
			long i = 1;
			// loop controls in pos order
			for (ControlPos c : controls) {
				if (!c.isW()) {
					String p = sol.getStr(iP, i);
					String r = c.getValue();
					
					// save answer to dta
					Data.storeStr(Data.getVarIndex(p),obs,r);
					
	        		Double w = sol.getNum(iW,i);						// w
	        		Double tipo = sol.getNum(iT,i);						// answer type
	        		Double punt = (double) 0;			// by default
	        		if (tipo==1) {
	        			// Val. Prof.
	        			if (r.equalsIgnoreCase("?")) {
	        				SFIToolkit.errorln("error getting PEC data: ? in " + p);
	        				return (198);	        				
	        			} else {
	        				punt = Double.parseDouble(r.replace(",", ".")) * w;	        				
	        			}
	        		} else {
	        			String rescor = sol.getStr(sol.getVarIndex("rescor"), i); 	// right answer
	        			// token in case there's more than one right answer
	        			String[] respuestas = rescor.split(";");
	        			for (String resp : respuestas) {
	        				if (tipo==2) {
	        					// closed answer
	        					if (r.trim().equalsIgnoreCase(resp.trim())) punt = w;
	        				}
	        				if (tipo==3) {
	        					// numeric answer
	        					try {  
	        						if (Double.parseDouble(r.replace(",",".")) == 
	        								Double.parseDouble(resp.replace(",","."))) punt = w;
	        					} catch(NumberFormatException e){
	        						// if the answer can't be a number, it is wrong
	        					}
	        				}        				
	        			}
	        		}
	        		Data.storeNum(Data.getVarIndex(p.replace("P", "w")),obs,punt);
					
					i++;
				}
			}
		} catch (Exception e) {
			SFIToolkit.errorln("error getting PEC data");
			SFIToolkit.errorln("(" + e.getMessage() + ")");
			return (198);
		}
		
		return (rc);
	}
	
	public static int getSolStata(String args[]) throws IOException {
		String dir = args[0];
		int rc = 0;
    	// Stata java extension
    	try {
    		List<ControlPos> controls = getControlsWithNames(dir);
    		SFIToolkit.executeCommand("clear",false);
    		Data.addVarStr("p",8);
    		Data.addVarInt("tipo");
    		Data.addVarFloat("w");
    		long obs = 1;
    		for (ControlPos c : controls) {
            	if (!c.isW()) {
            		SFIToolkit.executeCommand("set obs "+ String.valueOf(obs),false);
            		Data.storeStr(Data.getVarIndex("p"), obs, c.getName());
            		Data.storeNum(Data.getVarIndex("tipo"), obs, c.getTipo());
            		Data.storeNum(Data.getVarIndex("w"), obs, c.getW());
            		obs++;
            	}
            }
		} catch (Exception e) {
			SFIToolkit.errorln("error getting PEC data");
			SFIToolkit.errorln("(" + e.getMessage() + ")");
			return (198);
		}
        
        return (rc);
	}
	
	public static int getHonor(String args[]) {
		String file = args[0];
		String dni = args[1];
		int rc = 0;
		// Stata java extension
		try {
	        // open pdf form
			PDDocument pdf = PDDocument.load(new File(file));
		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
	        if (form.getFields().size()>0) {
	        	String c =(getCheckBoxHonor(form) ? "1" : "0");
        		SFIToolkit.executeCommand("qui replace honor = " + c + " if DNI==\""+dni+"\"",false);
	        } else {
	        	SFIToolkit.executeCommand("qui replace problema = 1 if DNI==\""+dni+"\"",false);
	        }
	        form = null;
	        pdf.close();
	        pdf = null;
		} catch (Exception e) {
			SFIToolkit.errorln("error getting PEC data");
			SFIToolkit.errorln("(" + e.getMessage() + ")");
			return (198);
		}

		return (rc);
	}

	public static int getPEC1(String args[]) {
		String pec = args[0];
		long obs = Long.parseLong(args[1]);
		int rc = 0;
		
		try {
			File f = new File(pec);
			if(f.exists() && f.isFile()) {
				// if the file exists, PEC1 is entregada
				Data.storeNum(Data.getVarIndex("ePEC1"), obs, 1);
				
                // open form
    			PDDocument pdf = PDDocument.load(f);
    			PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
    			// check honor
    			int ihonor =(getCheckBoxHonor(form) ? 1 : 0);
            	Data.storeNum(Data.getVarIndex("hPEC1"), obs, ihonor);
    	    	// get controls in order
            	// PEC1 structure is 10 numeric answers with equal weight
    			List<ControlPos> controls = getControls(pdf);
    			int j = 65;			// ASCII code 65 is A
    			for (int i = 0; i < controls.size(); i++) {
    				ControlPos c = controls.get(i);
    				if (!c.isW()) {
        				// answer var in Stata named R01_A, R01_B, ...
	    				String name = "R01_"+String.valueOf((char)j);
	    				String v = c.getValue();
	    				if (!v.isEmpty()) {
	    					Data.storeNum(Data.getVarIndex(name), obs, Double.parseDouble(v.replace(",", ".")));
	        			} else {
	        				Data.storeNum(Data.getVarIndex(name), obs, Missing.getValue());
	        			}
	    				j++;
    				}
    			}    			
    	        form = null;
    	        pdf.close();
    	        pdf = null;
			} else {
				// if the file doesn't exist, the PEC1 is no entregada
				Data.storeNum(Data.getVarIndex("ePEC1"), obs, 0);
			}
		} catch (Exception e) {
			SFIToolkit.errorln("error getting PEC1 data");
			SFIToolkit.errorln("(" + e.getMessage() + ")");
			return (198);
		}

		return (rc);
	}
	
	public static Boolean getCheckBoxHonor(PDAcroForm form) {
		Boolean honor = false;
    	Iterator<PDField> it = form.getFieldIterator();
    	while (it.hasNext()) {
    		PDField f = it.next();
    		if (!f.isReadOnly() && f instanceof PDCheckBox) {
            	PDCheckBox fhonor = (PDCheckBox) f;
            	honor = fhonor.isChecked();
    		}
    	}
		return (honor);
	}

	public static int getSintaxis(String args[]) {
		String orig = args[0];
		String sint = args[1];
		String cd = args[2];
		int rc = 0;
		
		try {
	        // get all pdf files from orig
	        File folder = new File(orig);
	        FilenameFilter pdfFilter = (File dir1, String name) -> { return name.toLowerCase().endsWith(".pdf"); };
	        File[] PECs = folder.listFiles(pdfFilter);

			int iP = Data.getVarIndex("p"); 
			int itipo = Data.getVarIndex("tipo");
			int iacc = Data.getVarIndex("accion"); 
			int ip1 = Data.getVarIndex("p1"); 
			int ip2 = Data.getVarIndex("p2"); 
			long total = Data.getObsTotal();			
			String curso = Data.getStrf(Data.getVarIndex("curso"), 1);
	        for (File file : PECs) {
	            if (file.isFile()) {
	            	// dni from file name
	                String name = file.getName();
	                String dni = name.substring(name.lastIndexOf("_")+1,name.lastIndexOf("."));

	                // open pdf form
	    			PDDocument pdf = PDDocument.load(file);
	    			PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();

	            	List<String> lines = new ArrayList<>();
	                lines.add("/* PEC: " + dni);
	    			// comments, if any
	    	        if (!form.getField("COMENT").getValueAsString().isEmpty()) {
                        lines.add(form.getField("COMENT").getValueAsString());
                    }
	                lines.add("*/");
	                
	                // working folder
	                lines.add("cd " + cd + "\n");
	                // ST2: delete files
	                if (curso.equalsIgnoreCase("ST2")) {
	                	String c = Data.getStrf(ip2,1);
	                	if (c.contains(";")) {
	                		c = c.split(";")[1];
	                		String[] del = c.split(" ");
	                		for (String d : del) {
	                			lines.add("capture erase " + d + ".dta");
	                		}
	                	}
	                	lines.add("scalar drop _all\n");
                		lines.add("clear\n");
	                }
	                
	                // get PEC data and build syntax do file
	    			List<ControlPos> controls = getControlsMulti(pdf);
	    			int iControl = 0;
	    			for (long i = 1; i <= total; i++ ) {
	    				Double tipo = Data.getNum(itipo, i);
	    				// only Val. Prof. fields (tipo = 1) generate syntax
	    				if (tipo==1) {
	    					ControlPos c = controls.get(iControl);
	    					iControl++;
	    					
	    					Double accion = Data.getNum(iacc, i);
    						// if action is not ignore (4), process
	    					if (accion!=4) {
	    						lines.add("** Pregunta " + Data.getStrf(iP, i).substring(1,3));
		    					if (accion==1 || accion==2) {
		    						// si read (1) or test (2), read field cotents
		    						String s = c.getValue();
		    						if (!s.isEmpty()) {
			    						lines.add(s+"\n");
			    						// it test (2) Test, add check code
			    						if (accion==2) {
			    							lines.add("preserve");
			    							String t = ( Data.getStrf(ip1,i).indexOf("_") >= 0 ? "* _*" : "*" );
			    							lines.add("rename " + t + ", lower");
			    							lines.add("capture noisily cf " + Data.getStr(ip1,i) +
			    									" using " + Data.getStr(ip2,i) + ", all verbose");
			    							lines.add("restore\n");
			    						}
		    						}
		    					}
		    					if (accion==3) {
		    						// if data (3), add syntax to open file
		    						String f = Data.getStrf(ip1, i);
		    						String ext = f.substring(f.lastIndexOf(".")+1);
		    						if (ext.equalsIgnoreCase("xlsx")) {
		    							lines.add("import excel " + f + ", sheet(" + Data.getStr(ip2, i).split(";")[0] +
		    									") firstrow clear\n");
		    						}
		    						if (ext.equalsIgnoreCase("dta")) {
		    							lines.add("use " + f + ", clear\n");
		    						}
		    					}
		    					if (accion==5) {
		    						// if save (5), add save command
		    						String f = Data.getStrf(ip1, i);
	    							lines.add("save " + f + ", replace\n");			    							
		    					}
	    					}
	    				}
	    			}
	    	        // save do file
	    	        Files.write(Paths.get(sint+ "/" + dni + ".do"), lines, Charset.forName("UTF-8"));
	    	        
	    	        form = null;
	    	        pdf.close();
	    	        pdf = null;
	            }
	        }
		} catch (Exception e) {
			SFIToolkit.errorln("error getting PEC data");
			SFIToolkit.errorln("(" + e.getMessage() + ")");
			return (198);
		}

		return (rc);
	}

	
	public static List<ControlPos> getControls(PDDocument pdf) {       
	    List<ControlPos> controls = new ArrayList<>();
	    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
    	Iterator<PDField> it = form.getFieldIterator();
    	while (it.hasNext()) {
    		PDField f = it.next();
			PDRectangle r = f.getWidgets().get(0).getRectangle();
            int p = pdf.getPages().indexOf(f.getWidgets().get(0).getPage());
            if (p>0 && (f instanceof PDTextField || f instanceof PDComboBox)) {
                Boolean lAdd = true;
            	float w = 0;
                if (f instanceof PDTextField) {
        			PDTextField ed = (PDTextField) f;
        			lAdd = !ed.isMultiline();
        			if (lAdd && ed.isReadOnly()) {
        				String s = ed.getValue().replace(",", ".");
        				w = Float.parseFloat(s.substring(s.indexOf("=")+1,s.indexOf(")")));
        			}
                }
                if (lAdd) {
                    ControlPos pos = new ControlPos(f,p,r,w);
                    // pos.setLowerY((792 - r.getUpperRightY()));
                    controls.add(pos);
                }
            }	    			
    	}
    	form = null;
		controls.sort(Comparator
			    .comparing(ControlPos::getPage)
			    .thenComparing(ControlPos::getY)
			    .thenComparing(ControlPos::getX));
		return (controls);
	}

	public static List<ControlPos> getControlsMulti(PDDocument pdf) {       
	    List<ControlPos> controls = new ArrayList<>();
	    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
    	Iterator<PDField> it = form.getFieldIterator();
    	while (it.hasNext()) {
    		PDField f = it.next();
            int p = pdf.getPages().indexOf(f.getWidgets().get(0).getPage());
            if (p>0 && (f instanceof PDTextField)) {
            	PDTextField ed = (PDTextField) f;
            	if (ed.isMultiline()) {
        			PDRectangle r = f.getWidgets().get(0).getRectangle();
                    ControlPos c = new ControlPos(f,p,r,0);
                    controls.add(c);            		
            	}
            }
    	}
    	form = null;
		controls.sort(Comparator
			    .comparing(ControlPos::getPage)
			    .thenComparing(ControlPos::getY)
			    .thenComparing(ControlPos::getX));
		return (controls);
	}
	
	public static List<ControlPos> getControlsWithNames(String dir) throws IOException {
    	List<ControlPos> controls = new ArrayList<>();
    	File file = new File(dir);
        if (file.isFile()) {
	        // open pdf form & get controls
			PDDocument pdf = PDDocument.load(file);
			controls = getControls(pdf);
	        pdf.close();
	        pdf = null;
				
            // loop through the sorted fields and count number of controls on each question
			int tot = controls.size();
			// first field is always W
			int iW = 1;
			ControlPos cW = controls.get(0);
			cW.setName(iW,0,0,"");
			int ncontrols = 0;
			for (int i = 1; i < tot; i++) {
				ControlPos c = controls.get(i);
                if (c.isW()) {
                	cW.setNControls(ncontrols);
                	
                	iW = iW + 1;
        			ncontrols = 0;
                	cW = c;
        			cW.setName(iW,0,0,"");
                } else {
                	ncontrols++;
                }
			}
			// set last ncontrols
			cW.setNControls(ncontrols);
			
	        // name controls
	        String pre = "";
	        iW = 0;
	        int n = 0;
	        int iCol = 0;
	        int iRow = 64;			// ASCII code for letter A is 65
	        for (int i = 0; i < tot; i++) {
	        	/* names: rows with letters (A,B,..), cols (if +1) with numbers (1,2,...) for each question 
	        	 * P01_A1 P01_A2 P01_A3
	        	 * P02_A
	        	 * P03_A1 P03_A2
	        	 * P03_B1 P03_B2 P03_B3
	        	 */
	        	ControlPos c = controls.get(i);
	        	if (c.isW()) {
	        		iW++;
	        		cW = c;
	    	        pre = "";
	        		n = 0;
	        		iRow = 64;
	        		iCol = 0;
	        	} else {
	        		n = n +1;
	        		if (controls.get(i-1).getY() < c.getY()) {
	        			// actual control is at an Y pos > than the previous: add one letter
	        			iRow = iRow + 1;
	        			iCol = 0;			// by default, no row
	        			if (i<(tot-1)) {
	        				if (controls.get(i+1).getY() == c.getY()) {
	        					iCol = 1;		// actual control is the first of a row
	        				}
	        			}
	        			if (iRow > 90) {
	        				pre = "Z";	// if more than Z rows, add Z at the beginning and start anew: ZA, ZB,..
	        				iRow = 65;
	        			}
	        		} else {
	        			iCol = iCol + 1;		// actual control is the next on a row
	        		}
	        		c.setName(iW,iRow,iCol,pre);
	        		// compute weight of each control: total W / ncontrols
	        		double d = cW.getW() / cW.getNControls();
	        		d = Math.floor((d) * 10000) / 10000;		// truncate to 4 decimals
	        		if (n == cW.getNControls() && cW.getNControls() > 1) {
	        			// if there's +1 items, the last one gets the extra weight
	        			d = cW.getW() - (d*(cW.getNControls()-1));
	        			d = Math.round(d * 10000.0) / 10000.0;		// round to 4 decimals
	        		}
	        		c.setW(d);
	        	}
	        }
        }
	        
        return (controls);
	}
	
	public static void checkPEC(String dir) throws IOException {
		
	    // fill PEC fields with string values
		PDDocument pdf = PDDocument.load(new File(dir));
	    List<ControlPos> controls = getControls(pdf);
	    for (ControlPos control:controls) {
    		control.fill();
	    }
	    
	    // fill MutliLine PEC fields with string values
		// load the pdf & the form
		List<ControlPos> controlsM = getControlsMulti(pdf);
	    for (ControlPos controlM : controlsM) {
    		controlM.fill();
	    }
	    
		// check for fixed fields
		PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
		String fixed = "";
		for (String n : "APE1 APE2 NOMBRE DNI HONOR COMENT".split(" ")) {
	    	try {
	    		if (n.equalsIgnoreCase("HONOR")) {
	    			PDCheckBox ch = (PDCheckBox) form.getField(n);
	    			ch.check();
	    		} else {
		    		PDTextField field = (PDTextField) form.getField(n);
		    		if (n.equalsIgnoreCase("APE1")) field.setValue("PRIMER APELLIDO");
		    		if (n.equalsIgnoreCase("APE2")) field.setValue("SEGUNDO APELLIDO");
		    		if (n.equalsIgnoreCase("NOMBRE")) field.setValue("NOMBRE");
		    		if (n.equalsIgnoreCase("DNI")) field.setValue("12345678X");
		    		if (n.equalsIgnoreCase("COMENT")) field.setValue("COMENTARIO");			    			
	    		}
	    	} catch (Exception e) {
	    		fixed = fixed + (fixed.length()>0 ? ";" : "") + n;
    		}
		}
		
		// save pdf as a copy
		pdf.save(dir.replace(".pdf","_campos.pdf"));
		pdf.close();
		pdf = null;
		form = null;

		// check field overlapping and build names txt
	    controls = getControlsWithNames(dir);
	    int tot = controls.size();
		String over = "";
		String intro = System.lineSeparator();
		// first control is always W
		String c = "*** Pag. " + (controls.get(0).getPage()+1);
		c = c + intro + "* " + controls.get(0).getName();
		for (int i = 1; i < tot; i++) {
			ControlPos control = controls.get(i);
			ControlPos prev = controls.get(i-1);
			if (control.getPage() != prev.getPage()) 
				c = c + intro + "*** Pag. " + (control.getPage()+1);

			Boolean lLine = control.getY() == prev.getY();
			c = c + (lLine ? " " : intro) + (control.isW() ? "* " : "") + control.getName();
			
			// overlapping may only occur in the same page
			if (prev.isOverlapping(control.getY(),control.getPage())) {
				over = over + (over.length()>0 ? ";" : "") + control.getName();
			}
		}
		PrintWriter out = new PrintWriter(dir.replace(".pdf","_campos.txt"));
		out.print(c);
		out.close();
		
		// final message
		String warning = "";
		if (!fixed.isEmpty() || !over.isEmpty()) {
			warning = "ATENCIÓ!!!" + intro + 
					(fixed.length()>0   ? "   No s'ha(n) trobat el(s) camp(s): " + fixed + intro : "") +
					(over.length()>0  ? "   Controls mal alineats: " + over + intro : "") +
					intro; 
		}
		JOptionPane.showMessageDialog(null,"Procés completat" + 
					(!warning.isEmpty() ? " amb ERRORS." : ".") + intro + warning +
				"S'han creat els fitxers:" + intro +
				dir.replace(".pdf","_campos.pdf") + intro +
				dir.replace(".pdf","_campos.txt"),"Comprovar PEC",
				(warning.isEmpty() ? JOptionPane.PLAIN_MESSAGE : JOptionPane.ERROR_MESSAGE));					
	}
	
	public void getNotaPEC0(String dir) throws IOException { 
	    try {
			PDDocument pdf = PDDocument.load(new File(dir));
		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
			PDComboBox co = (PDComboBox) form.getField("NOTA");
			File f = new File(dir);
			File t = new File(f.getParent() + "/NOTA.txt");
			FileUtils.writeStringToFile(t, co.getValue().get(0), Charset.defaultCharset());            
            // close pdf form
            pdf.close();
            pdf = null;
            form = null;
	    } catch (Exception e) {
			e.printStackTrace();
		}
	}
		
    public void getEntregaPEC0(String dir, String periodo) throws IOException {  
        //Get the folders of the original directory dir
        File orig = new File(dir);
        String[] directories = orig.list(new FilenameFilter() {
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        
        //Loop thorugh the folders
        boolean lProblems = false;
        List<String> lines = new ArrayList<String>();
        List<String> problems = new ArrayList<String>();
        lines.add("DNI;Curso;Periodo;npec;entregada;honor;mdb;pdf");
        try {
	        for (String f : directories) {
	            String dni = f.substring(f.lastIndexOf("_")+1);     //student's dni
	            
	            //Get list of files for the student and confirm PEC1 elements
	            boolean foundMdb = false;
	            boolean foundPdf = false;
	            boolean honor = false;
	            
	            File folder = new File(dir + "/" + f);
		        File[] listOfFiles = folder.listFiles();
		        for (File file : listOfFiles) {
		            if (file.isFile()) {
	                    String n = file.getName();
	                    String ext = n.substring(n.lastIndexOf(".")+1);     //file extension
	                    
	                    //there's a database
	                    if (ext.equals("mdb") || ext.equals("accdb") || ext.equals("odb")) foundMdb = true;
	                    
	                    //there's a pdf form file
	                    if (ext.equals("pdf")) {
	                        foundPdf = true;
	                        
	                        //open pdf file
	            			PDDocument pdf = PDDocument.load(file);
	            		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
	            		    String producer = pdf.getDocumentInformation().getProducer();		// get form producer
	
	                        if (producer.toUpperCase().contains("LibreOffice".toUpperCase()) ||
	                                form.getFields().size()>0) {
	                            //get honor field
	                            PDCheckBox cbHonor = (PDCheckBox) form.getField("HONOR");
	                            honor = cbHonor.isChecked();
	                        } else {
	                            //the pdf is not readable
	                            lProblems = true;
	                            problems.add(dni);
	                        }
	                        
	                        // close pdf form
	                        pdf.close();
	                        pdf = null;
	                        form = null;
	                    }
	                }
	            }
	
	            // curso = ST1, npec = 1, entregada = 1 always; add mdb, pdf, honor information to data
	            String c = "'" + dni + "';'ST1';'" + periodo + "';1;1;" +
	            		((honor) ? "1" : "0") + ";" +
	            		((foundMdb) ? "1" : "0") + ";" +
	            		((foundPdf) ? "1" : "0"); 
	            lines.add(c);
	        }
	        //write pec1 data file
	        Path fdata = Paths.get(dir + "/datos_pec1.txt");
	        Files.write(fdata, lines, Charset.forName("UTF-8"));
	        //write problems file
	        if (lProblems) {
	            Path fproblems = Paths.get(dir + "/problemas.txt");
	            Files.write(fproblems, problems, Charset.forName("UTF-8"));
	            JOptionPane.showMessageDialog(null,"Hay problemas.");
	        }
        } catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	public void test(String dir) throws IOException {
		// test method
	}

	
	public static void main(String args[]) {
		// DO NOTHING
	}
	
/*
 * OLD METHODS
 * 
 	public void getDatosSol(String dir, String sol) throws IOException { 
        // get all the pdf files of dir
        File folder = new File(dir);
        FilenameFilter pdfFilter;
        pdfFilter = (File dir1, String name) -> { return name.toLowerCase().endsWith(".pdf"); };
        File[] PECs = folder.listFiles(pdfFilter);

        boolean lProblems = false;
        boolean lComments = false;
        boolean lfirst = true;
        boolean lhonor = false;
        boolean lcomentarios = false;
        boolean lape1 = false;
        boolean lape2 = false;
        boolean lnom = false;
        List<String> lines = new ArrayList<>();
        List<String> comments = new ArrayList<>();
        List<String> problems = new ArrayList<>();
        List<String> names = new ArrayList<>();
        for (File file : PECs) {
            if (file.isFile()) {
            	// get dni from filename
                String n = file.getName();
                String dni = n.substring(n.lastIndexOf("_")+1,n.lastIndexOf("."));
                
                System.out.println(dni);

                // open pdf form
				PDDocument pdf = PDDocument.load(file);
			    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
                
			    String producer = pdf.getDocumentInformation().getProducer();		// get form producer                
                if (form.getFields().size()>0) {
                    if (!producer.substring(0,Math.min(11,producer.length())).equalsIgnoreCase("LibreOffice")) {
                    	// if the producer is not LibreOffice, the PDF file may be corrupted
                    	lProblems = true;
                        problems.add(dni + "; " + producer);
                    }
                    
                    if (lfirst) {
                        // get form fields names and sort alphabetically
        				for (PDField f : form.getFields()) {
        					String name = f.getFullyQualifiedName();
        					if (name.equalsIgnoreCase("APE1")) lape1 = true;						// there's APE1 field
        					if (name.equalsIgnoreCase("APE2")) lape2 = true;						// there's APE2 field
        					if (name.equalsIgnoreCase("NOMBRE")) lnom = true;						// there's NOMBRE field
        					if (name.equalsIgnoreCase("HONOR")) lhonor = true;						// there's HONOR field
        					if (name.equalsIgnoreCase("COMENT")) lcomentarios = true;				// there's COMENT field
        				}
        				// read sol txt line by line and get names
        				File f= new File(sol);
        				BufferedReader b = new BufferedReader(new FileReader(f));
        				String line = "";
        	            while ((line = b.readLine()) != null) {
        	            	if (!line.substring(0,2).equalsIgnoreCase("id")) {    
        	            		String t[] = line.split(",");
        	            		names.add(t[1].replace("'",""));
        	            	}
        	            }
        	            b.close();
        	            b = null;
                        lfirst = false;
                    }
                    
                    if (lcomentarios) {
	                    // build COMMENTS section
	                    if (!form.getField("COMENT").getValueAsString().isEmpty()) {
	                        lComments = true;
	                        comments.add(dni + ":" + form.getField("COMENT").getValueAsString() + "\n");
	                    }
                    }
                    // header with identification data
                    String c = (lape1 ? "'" + form.getField("APE1").getValueAsString() + "'" : "null") + "," +
                    		(lape2 ? "'" + form.getField("APE2").getValueAsString() + "'" : "null") + "," +
                    		(lnom ? "'" + form.getField("NOMBRE").getValueAsString() + "'" : "null") + "," +
                    		"'" + dni + "'";
                    if (lhonor) {
                    	PDCheckBox honor = (PDCheckBox) form.getField("HONOR");
                        c = c + (honor.isChecked() ? ",1" : ",0");
                    }

                    // loop through the sol answers and get the contents
                    for (String name : names) {
                    	PDField f = form.getField(name);
                		if (f instanceof PDTextField) {
                			PDTextField ed = (PDTextField) f;				// text field: numeric or memo
                			c = c + ",'" + ed.getValue().replace(".", ",") + "'";
                		}
                		if (f instanceof PDComboBox) {
                			PDComboBox co = (PDComboBox) f;					// combobox field: closed answer
                			c = c + ",'" + co.getValue().get(0) + "'";
                		}
                    }
                    lines.add(c);                    
                } else {
                	// if there are no fields on the form the PDF file may be corrupted
                    lProblems = true;
                    if (form.getFields().isEmpty()) {
                        problems.add(dni + "; no fields");
                    }
                }
                
                // close pdf form
                pdf.close();
                pdf = null;
                form = null; 
            }
        }

        // save data
        Files.write(Paths.get(dir + "/datos_pecs.txt"), lines, Charset.forName("UTF-8"));
        // save comments, if any
        if (lComments) Files.write(Paths.get(dir + "/comentarios.txt"), comments, Charset.forName("UTF-8"));
        // save problems, if any
        if (lProblems) Files.write(Paths.get(dir + "/errores.txt"), problems, Charset.forName("UTF-8"));

        JOptionPane.showMessageDialog(null, "Proceso finalizado." +
                (lComments ? " Hay comentarios." : "") + 
                (lProblems ? " Hay errores." : ""));
	}

	public void getP(String dir, String periodo, String curso) throws IOException {

		// build INSERT INTO sql with dir PEC data
        try {
        	File f = new File(dir); 
        	String n = f.getName();
        	String dni = n.substring(n.lastIndexOf("_") + 1, n.indexOf(".pdf"));
        	String sql = "";
			PDDocument pdf = PDDocument.load(f);
		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
		    for(PDField field : form.getFields()){
		    	String name = field.getPartialName();
		    	String v = "";
		    	if (name.substring(0,1).equals("P")) {
            		if (field instanceof PDTextField) {
            			PDTextField ed = (PDTextField) field;				// text field: numeric or memo
            			v = ed.getValue().replace(".", ",");
            		}
            		if (field instanceof PDComboBox) {
            			PDComboBox co = (PDComboBox) field;					// combobox field: closed answer
            			v = co.getValue().get(0);
            		}

                	sql = sql + "INSERT INTO pec_respuestas (Periodo, Curso, DNI, Pregunta, respuesta) " +
                			"VALUES ('" + periodo + "','" + curso + "','" + dni + "','" + name.substring(1) + "','" + v + "');" +
                			System.lineSeparator();
		    	}
		    }
		    // copy sql sentence to clipboard
		    toClip(sql);
		    
            // close pdf form
            pdf.close();
            pdf = null;
            form = null;
        } catch (Exception e) {
			e.printStackTrace();
        	JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
    public void getEntregaHonorIO(String dir, String periodo, String curso) throws IOException {
        //Get the folders of dir
        File folder = new File(dir);
        FileFilter folderFilter = new FileFilter() {
            public boolean accept(File file) {
                return !file.isFile();
            }
        };
        
        List<String> lines = new ArrayList<>();
        List<String> problems = new ArrayList<>();
        Boolean lProblems = false;
        
        File[] folders = folder.listFiles(folderFilter);
        for(File f : folders) {
        	String n = f.getName();
        	String dni = n.substring(n.lastIndexOf("_") + 1);
        	
            //Get the PDF files of dir
            File subfolder = new File(f.getAbsolutePath());
            FilenameFilter pdfFilter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".pdf");
                }
            };
            
            try {
                boolean lError = true;
	            boolean honor = false;
		        File[] listOfFiles = subfolder.listFiles(pdfFilter);
		        for (File file : listOfFiles) {
		            if (file.isFile()) {
	                    //open pdf file
		    			PDDocument pdf = PDDocument.load(file);
		    		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
		    		    String producer = pdf.getDocumentInformation().getProducer();		// get form producer
		    		    if (form != null) {
		                	if (producer.toUpperCase().contains("LibreOffice".toUpperCase()) ||
		                        form.getFields().size()>0) {	                	
		                        //get honor field
		                        PDCheckBox cbHonor = (PDCheckBox) form.getField("HONOR");
		                        honor = cbHonor.isChecked();
		                        lError = false;
	                        }
	                    }
	                    // close pdf form
	                    pdf.close();
	                    pdf = null;
	                    form = null;
		            }
		        }
		        
                if (!lError) {
    	            String c = dni + ";" +  ((honor) ? "1" : "0"); 
    	            lines.add(c);
                } else {
                	lProblems = true;
                    problems.add(dni);               	
                }
            } catch (Exception e) {
    			e.printStackTrace();
    		}
        }
        
        //write data file
        Path fdata = Paths.get(dir + "/datos_pec.txt");
        Files.write(fdata, lines, Charset.forName("UTF-8"));
        //write problems file
        if (lProblems) {
            Path fproblems = Paths.get(dir + "/problemas.txt");
            Files.write(fproblems, problems, Charset.forName("UTF-8"));
            JOptionPane.showMessageDialog(null,"Hay problemas.");
        }

    }

	 	
	public void getnames_error(String dir) throws IOException { 
        // get all the pdf files of dir
        File folder = new File(dir);
        FilenameFilter pdfFilter;
        pdfFilter = (File dir1, String name) -> { return name.toLowerCase().endsWith(".pdf"); };
        File[] PECs = folder.listFiles(pdfFilter);

        boolean lProblems = false;
        boolean lComments = false;
        boolean lfirst = true;
        boolean lhonor = false;
        boolean lcomentarios = false;
        boolean lape1 = false;
        boolean lape2 = false;
        boolean lnom = false;
        List<String> lines = new ArrayList<>();
        List<String> mlines = new ArrayList<>();
        List<String> comments = new ArrayList<>();
        List<String> problems = new ArrayList<>();
        List<String> names = new ArrayList<>();
        List<String> memos = new ArrayList<>();
        
        String[] err = { "P02_D1_2", "P02_D2_2", "P02_D3_2", "P02_D4_2", "P02_D5_2", "P02_D6_2", "P02_D7_2", "P02_D8_2", 
        				"P02_D7_3", "P02_D8_3", "P05_B1_2", "P05_B2_2", "P05_B3_2" };
        String[] cor = { "P02_E01", "P02_E02", "P02_E03", "P02_E04", "P02_E05", "P02_E06", "P02_E07", "P02_E08", 
				"P02_E09", "P02_E10", "P08_B1", "P08_B2", "P08_B3" };
        
        for (File file : PECs) {
            if (file.isFile()) {
            	// get dni from filename
                String n = file.getName();
                String dni = n.substring(n.lastIndexOf("_")+1,n.lastIndexOf("."));
                
                System.out.println(dni);

                // open pdf form
				PDDocument pdf = PDDocument.load(file);
			    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
                
			    String producer = pdf.getDocumentInformation().getProducer();		// get form producer                
                if (form.getFields().size()>0) {
                    if (!producer.substring(0,Math.min(11,producer.length())).equalsIgnoreCase("LibreOffice")) {
                    	// if the producer is not LibreOffice, the PDF file may be corrupted
                    	lProblems = true;
                        problems.add(dni + "; " + producer);
                    }
                    
                    if (lfirst) {
                        // get form fields names and sort alphabetically
        				for (PDField f : form.getFields()) {
        					String name = f.getFullyQualifiedName();
        					// search for corrected names
        					if (Arrays.asList(err).contains(name)) {
        						name = cor[Arrays.asList(err).indexOf(name)];        						
        					}
        					if (name.substring(0, 1).equalsIgnoreCase("P")) names.add(name);		// answers
        					if (name.substring(0, 1).equalsIgnoreCase("M")) memos.add(name);		// memo fields
        					if (name.equalsIgnoreCase("APE1")) lape1 = true;						// there's APE1 field
        					if (name.equalsIgnoreCase("APE2")) lape2 = true;						// there's APE2 field
        					if (name.equalsIgnoreCase("NOMBRE")) lnom = true;						// there's NOMBRE field
        					if (name.equalsIgnoreCase("HONOR")) lhonor = true;						// there's HONOR field
        					if (name.equalsIgnoreCase("COMENT")) lcomentarios = true;				// there's COMENT field
        				}
                        Collections.sort(names);
                        Collections.sort(memos);
                        lfirst = false;
                    }
                    
                    if (lcomentarios) {
	                    // build COMMENTS section
	                    if (!form.getField("COMENT").getValueAsString().isEmpty()) {
	                        lComments = true;
	                        comments.add(dni + ":" + form.getField("COMENT").getValueAsString() + "\n");
	                    }
                    }
                    // header with identification data
                    String c = (lape1 ? "'" + form.getField("APE1").getValueAsString() + "'" : "null") + "," +
                    		(lape2 ? "'" + form.getField("APE2").getValueAsString() + "'" : "null") + "," +
                    		(lnom ? "'" + form.getField("NOMBRE").getValueAsString() + "'" : "null") + "," +
                    		"'" + dni + "'";
                    if (lhonor) {
                    	PDCheckBox honor = (PDCheckBox) form.getField("HONOR");
                        c = c + (honor.isChecked() ? ",1" : ",0");
                    }

                    // loop through the sorted answers and get the contents
                    for (String name : names) {
                    	// get the corrected field name
    					if (Arrays.asList(cor).contains(name)) {
    						name = err[Arrays.asList(cor).indexOf(name)];        						
    					}

                    	PDField f = form.getField(name);
                		if (f instanceof PDTextField) {
                			PDTextField ed = (PDTextField) f;				// text field: numeric or memo
                			c = c + ",'" + ed.getValue().replace(".", ",") + "'";
                		}
                		if (f instanceof PDComboBox) {
                			PDComboBox co = (PDComboBox) f;					// combobox field: closed answer
                			c = c + ",'" + co.getValue().get(0) + "'";
                		}
                    }
                    lines.add(c);
                    
                    if (!memos.isEmpty()) {
                    	// loop through the sorted memos and get the contents
                        String m = "'" + dni + "'";
	                    for (String name : memos) {
	                    	PDTextField ed = (PDTextField) form.getField(name);
	                		m = m + ",'" + ed.getValue().replace("'", "''") + "'";
                		}
	                    mlines.add(m);
                	}
                } else {
                	// if there are no fields on the form the PDF file may be corrupted
                    lProblems = true;
                    if (form.getFields().isEmpty()) {
                        problems.add(dni + "; no fields");
                    }
                }
                
                // close pdf form
                pdf.close();
                pdf = null;
                form = null; 
            }
        }

        // save data
        Files.write(Paths.get(dir + "/datos_pecs.txt"), lines, Charset.forName("UTF-8"));
        // save comments, if any
        if (lComments) Files.write(Paths.get(dir + "/comentarios.txt"), comments, Charset.forName("UTF-8"));
        // save problems, if any
        if (lProblems) Files.write(Paths.get(dir + "/errores.txt"), problems, Charset.forName("UTF-8"));
        // save memos, if any
        if (!memos.isEmpty()) Files.write(Paths.get(dir + "/memos.txt"), mlines, Charset.forName("UTF-8"));

        JOptionPane.showMessageDialog(null, "Proceso finalizado." +
                (lComments ? " Hay comentarios." : "") + 
                (lProblems ? " Hay errores." : ""));
	}

	public void getPIO1(String dir, String periodo, String curso) throws IOException {

		// build INSERT INTO sql with dir PEC data
        try {
        	File f = new File(dir); 
        	String n = f.getName();
        	String dni = n.substring(n.lastIndexOf("_") + 1, n.indexOf(".pdf"));
        	String sql = "";
			PDDocument pdf = PDDocument.load(f);
		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
		    for(PDField field : form.getFields()){
		    	String name = field.getPartialName();
		    	String v = "";
		    	if (name.substring(0,1).equals("P") && name.substring(name.length()-1).equals("A")) {
		    		// only the P..._A fields are of interest on IO1
            		if (field instanceof PDTextField) {
            			PDTextField ed = (PDTextField) field;				// text field: numeric or memo
            			v = ed.getValue().replace(".", ",");
            		}
            		if (field instanceof PDComboBox) {
            			PDComboBox co = (PDComboBox) field;					// combobox field: closed answer
            			v = co.getValue().get(0);
            		}

                	sql = sql + "INSERT INTO pec_respuestas (Periodo, Curso, DNI, Pregunta, respuesta) " +
                			"VALUES ('" + periodo + "','" + curso + "','" + dni + "','" + name.substring(1) + "','" + v + "');" +
                			System.lineSeparator();
		    	}
		    }
		    // copy sql sentence to clipboard
		    toClip(sql);
		    
            // close pdf form
            pdf.close();
            pdf = null;
            form = null;
        } catch (Exception e) {
			e.printStackTrace();
        	JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	public void getRespuestasIO(String dir, String periodo, String curso) throws IOException {
        //Get the PEC files of dir
        File folder = new File(dir);
        FilenameFilter pdfFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".pdf");
            }
        };

        List<String> lines = new ArrayList<>();
        try {
	        lines.add("periodo;curso;dni;q;n;resp");
	        File[] listOfFiles = folder.listFiles(pdfFilter);
	        for (File file : listOfFiles) {
	            if (file.isFile()) {
	            	String n = file.getName();
	            	String dni = n.substring(n.lastIndexOf("_") + 1, n.indexOf(".pdf"));
	            	String head = "'" + periodo + "';'" + curso + "';'" + dni + "';"; 

                	PDDocument pdf = PDDocument.load(file);
	    		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
                	for (PDField field : form.getFields()){
	    		    	String name = field.getPartialName();
	    		    	if (name.substring(0,1).equals("M") || name.substring(0,1).equals("P")) { 
	    		    		if (!name.substring(name.length()-1).equals("A")) {
		    		    		String c = field.getValueAsString();
		    		    		if (!(name.substring(0,1).equals("M") && 
		    		    				c.replace(",", ".").matches("-?\\d+(\\.\\d+)?"))) {
		    		    			lines.add(head + "'" + name.substring(1) + "';" +
		    		    				name.substring(1,3) + ";'" + c.replace("'", "$") + "'");
		    		    		}
	    		    		}
	    		    	}
	    		    }
	    			pdf.close();
	    			pdf = null;
                    form = null;
	            }
	        }
	        
	        Files.write(Paths.get(dir + "/respuestas.txt"), lines, Charset.forName("UTF-8"));
	        
	        System.out.println("End");
        } catch (Exception e) {
			e.printStackTrace();
		}
	}	

	public void getEntregaHonor(String dir, String periodo, String curso) throws IOException {  
        
        //Get the PEC files of dir
        File folder = new File(dir);
        FilenameFilter pdfFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".pdf");
            }
        };
        
        List<String> lines = new ArrayList<>();
        List<String> problems = new ArrayList<>();
        Boolean lProblems = false;
        try {
	        lines.add("DNI;Curso;Periodo;npec;entregada;honor");
	        File[] listOfFiles = folder.listFiles(pdfFilter);
	        for (File file : listOfFiles) {
	            if (file.isFile()) {
	            	String n = file.getName();
	            	String dni = n.substring(n.lastIndexOf("_") + 1, n.indexOf(".pdf"));
		            boolean honor = false;
		            
		            System.out.println(dni);
	            	
                    //open pdf file
	    			PDDocument pdf = PDDocument.load(file);
	    		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
	    		    String producer = pdf.getDocumentInformation().getProducer();		// get form producer
	                Boolean lError = true;
	    		    if (form != null) {
	                	if (producer.toUpperCase().contains("LibreOffice".toUpperCase()) ||
	                        form.getFields().size()>0) {	                	
	                        //get honor field
	                        PDCheckBox cbHonor = (PDCheckBox) form.getField("HONOR");
	                        honor = cbHonor.isChecked();
	                        
	                        lError = false;
                        }
                    }
                    // close pdf form
                    pdf.close();
                    pdf = null;
                    form = null;

                    if (!lError) {
	    	            // entregada = 1 always
	    	            String c = "'" + dni + "';'" + curso + "';'" + periodo + "';" +
	    	            		((curso.equalsIgnoreCase("ST1")) ? "2" : "1") + ";1;" +
	    	            		((honor) ? "1" : "0"); 
	    	            lines.add(c);
                    } else {
                    	lProblems = true;
                        problems.add(dni);                    	
                    }
	            }
	
	        }
	        //write data file
	        Path fdata = Paths.get(dir + "/datos_pec.txt");
	        Files.write(fdata, lines, Charset.forName("UTF-8"));
	        //write problems file
	        if (lProblems) {
	            Path fproblems = Paths.get(dir + "/problemas.txt");
	            Files.write(fproblems, problems, Charset.forName("UTF-8"));
	            JOptionPane.showMessageDialog(null,"Hay problemas.");
	        }
        } catch (Exception e) {
			e.printStackTrace();
		}
    }

	public void getMemos(String dir, String periodo, String curso) throws IOException {
        //Get the PEC files of dir
        File folder = new File(dir);
        FilenameFilter pdfFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".pdf");
            }
        };

        List<String> lines = new ArrayList<>();
        List<String> error = new ArrayList<>();
        Boolean lProblemas = false;
        try {
	        lines.add("periodo;curso;dni;pregunta;respuesta");
	        File[] listOfFiles = folder.listFiles(pdfFilter);
	        for (File file : listOfFiles) {
	            if (file.isFile()) {
	            	String n = file.getName();
	            	String dni = n.substring(n.lastIndexOf("_") + 1, n.indexOf(".pdf"));
	            	
	    			PDDocument pdf = PDDocument.load(file);
	    		    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
	    		    String producer = pdf.getDocumentInformation().getProducer();		// get form producer
	                if (producer.toUpperCase().contains("LibreOffice".toUpperCase()) ||
	                        form.getFields().size()>0) {
		    		    for(PDField field : form.getFields()){
		    		    	String name = field.getPartialName();
		    		    	if (name.substring(0,1).equals("M")) {
		    		    		String c = field.getValueAsString();
		    		    		c = c.replace("'", "$");
		    		    		lines.add("'" + periodo + "';'" + curso + "';'" + dni + "';'" + 
		    		    				name + "';'" + c  + "'");
		    		    	}    		    	
		    		    }
	                } else {
	                	lProblemas = true;
	                	error.add(dni + "; " + producer);
	                }
	                
                    // close pdf form
                    pdf.close();
                    pdf = null;
                    form = null;
	            }
	        }
	        
	        Files.write(Paths.get(dir + "/respuestas.txt"), lines, Charset.forName("UTF-8"));
	        if (lProblemas) {
	        	Files.write(Paths.get(dir + "/errores.txt"), error, Charset.forName("UTF-8"));
	        	JOptionPane.showMessageDialog(null,"Hay problemas.");
	        }
        } catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getDatosGeneral0(String dir) throws IOException { 
        // get all the pdf files of dir
        File folder = new File(dir);
        FilenameFilter pdfFilter;
        pdfFilter = (File dir1, String name) -> { return name.toLowerCase().endsWith(".pdf"); };
        File[] PECs = folder.listFiles(pdfFilter);

        boolean lProblems = false;
        boolean lComments = false;
        boolean lfirst = true;
        boolean lhonor = false;
        boolean lcoment = false;
        boolean lape1 = false;
        boolean lape2 = false;
        boolean lnom = false;
        List<String> lines = new ArrayList<>();
        List<String> mlines = new ArrayList<>();
        List<String> comments = new ArrayList<>();
        List<String> problems = new ArrayList<>();
        for (File file : PECs) {
            if (file.isFile()) {
            	// get dni from filename
                String n = file.getName();
                String dni = n.substring(n.lastIndexOf("_")+1,n.lastIndexOf("."));
                
                System.out.println(dni);

                // open pdf form
				PDDocument pdf = PDDocument.load(file);
			    PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
                
			    String producer = pdf.getDocumentInformation().getProducer();		// get form producer                
                if (form.getFields().size()>0) {
                    if (!producer.substring(0,Math.min(11,producer.length())).equalsIgnoreCase("LibreOffice")) {
                    	// if the producer is not LibreOffice, the PDF file may be corrupted
                    	lProblems = true;
                        problems.add(dni + "; " + producer);
                    }
                    
                    if (lfirst) {
                        // get form fields names and sort alphabetically
        				for (PDField f : form.getFields()) {
        					String name = f.getFullyQualifiedName();
        					if (name.substring(0, 1).equalsIgnoreCase("P")) names.add(name);		// answers
        					if (name.substring(0, 1).equalsIgnoreCase("M")) memos.add(name);		// memo fields
        					if (name.equalsIgnoreCase("APE1")) lape1 = true;						// there's APE1 field
        					if (name.equalsIgnoreCase("APE2")) lape2 = true;						// there's APE2 field
        					if (name.equalsIgnoreCase("NOMBRE")) lnom = true;						// there's NOMBRE field
        					if (name.equalsIgnoreCase("HONOR")) lhonor = true;						// there's HONOR field
        					if (name.equalsIgnoreCase("COMENT")) lcomentarios = true;				// there's COMENT field
        				}
                        Collections.sort(names);
                        Collections.sort(memos);
                        lfirst = false;
                    }
                    
                    if (lcomentarios) {
	                    // build COMMENTS section
	                    if (!form.getField("COMENT").getValueAsString().isEmpty()) {
	                        lComments = true;
	                        comments.add(dni + ":" + form.getField("COMENT").getValueAsString() + "\n");
	                    }
                    }
                    // header with identification data
                    String c = (lape1 ? "'" + form.getField("APE1").getValueAsString() + "'" : "null") + "," +
                    		(lape2 ? "'" + form.getField("APE2").getValueAsString() + "'" : "null") + "," +
                    		(lnom ? "'" + form.getField("NOMBRE").getValueAsString() + "'" : "null") + "," +
                    		"'" + dni + "'";
                    if (lhonor) {
                    	PDCheckBox honor = (PDCheckBox) form.getField("HONOR");
                        c = c + (honor.isChecked() ? ",1" : ",0");
                    }

                    // loop through the sorted answers and get the contents
                    for (String name : names) {
                    	PDField f = form.getField(name);
                		if (f instanceof PDTextField) {
                			PDTextField ed = (PDTextField) f;				// text field: numeric or memo
                			c = c + ",'" + ed.getValue().replace(".", ",") + "'";
                		}
                		if (f instanceof PDComboBox) {
                			PDComboBox co = (PDComboBox) f;					// combobox field: closed answer
                			c = c + ",'" + co.getValue().get(0) + "'";
                		}
                    }
                    lines.add(c);
                    
                    if (!memos.isEmpty()) {
                    	// loop through the sorted memos and get the contents
                        String m = "'" + dni + "'";
	                    for (String name : memos) {
	                    	PDTextField ed = (PDTextField) form.getField(name);
	                		m = m + ",'" + ed.getValue().replace("'", "''") + "'";
                		}
	                    mlines.add(m);
                	}
                } else {
                	// if there are no fields on the form the PDF file may be corrupted
                    lProblems = true;
                    if (form.getFields().isEmpty()) {
                        problems.add(dni + "; no fields");
                    }
                }
                
                // close pdf form
                pdf.close();
                pdf = null;
                form = null; 
            }
        }

        // save data
        Files.write(Paths.get(dir + "/datos_pecs.txt"), lines, Charset.forName("UTF-8"));
        // save comments, if any
        if (lComments) Files.write(Paths.get(dir + "/comentarios.txt"), comments, Charset.forName("UTF-8"));
        // save problems, if any
        if (lProblems) Files.write(Paths.get(dir + "/errores.txt"), problems, Charset.forName("UTF-8"));
        // save memos, if any
        if (!memos.isEmpty()) Files.write(Paths.get(dir + "/memos.txt"), mlines, Charset.forName("UTF-8"));

        JOptionPane.showMessageDialog(null, "Proceso finalizado." +
                (lComments ? " Hay comentarios." : "") + 
                (lProblems ? " Hay errores." : ""));
	}

	public void toClip(String s) {
		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		StringSelection c = new StringSelection(s);
		clip.setContents(c, c);
	}

 */
}
