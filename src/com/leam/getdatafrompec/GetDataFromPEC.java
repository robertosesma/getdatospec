package com.leam.getdatafrompec;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;


public class GetDataFromPEC {

	public static void main(String[] args) throws IOException {

		Boolean lContinue = false;
		String command = "";
		String dir = "";
		if (args.length == 0) {
			// if no arguments, show user interface
	        Object[] opt = {"Comprovar PEC","Extreure dades","Generar solució"};        
	        int x = JOptionPane.showOptionDialog(null, "Què vols fer?", "PECs",
	               JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, opt, opt[0]);
	        if (x>=0) {
	        	JFileChooser fileChooser = new JFileChooser();
	        	// default directory: user home or, if available, jar directory
	        	String defdir = System.getProperty("user.home");		
    	    	try {
    	    		defdir = new File(GetDataFromPEC.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
    	    	} catch (Exception e) {
        			 // do nothing, keep default home directory
        		}
	        	fileChooser.setCurrentDirectory(new File(defdir));
	        	if (x==0 || x==2) {
		        	fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		        	fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Documentos PDF", "pdf"));
		        	fileChooser.setAcceptAllFileFilterUsed(false);	        		
	        	}
	        	if (x==1) {
		        	fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        	}
	        	int result = fileChooser.showOpenDialog(null);
	        	if (result == JFileChooser.APPROVE_OPTION) {
	        		dir = fileChooser.getSelectedFile().getAbsolutePath();
	        	    if (x==0) command = "check";
	        	    if (x==1) command = "get";
	        	    if (x==2) command = "getsol";
	        	    
	        	    lContinue = true;
	        	}
	        }
		} else {
			// command line call
			command = args[0];
			dir = args[1];
			lContinue = true;
		}
		
		if (lContinue) {
	        // get data from PECs to txt; params: folder dir with PECs
	        if (command.equals("get")) ExtractMethods.getDatos(dir);
	        // get txt with sol data; params: PDF dir with PEC definition
	        if (command.equals("getsol")) ExtractMethods.getSol(dir);
	        // check and fill PEC; params: PDF dir with PEC definition
	        if (command.equals("check")) ExtractMethods.checkPEC(dir);
	        
			ExtractMethods extract = new ExtractMethods();
	        // params: dir
	        if (command.equals("getNotaPEC0")) extract.getNotaPEC0(dir);
	        // params: dir, periodo
	        if (command.equals("entregaPEC0")) extract.getEntregaPEC0(dir, args[2]);
	        // test
	        // if (command.equals("test")) extract.test(dir);
		}
	}

	
/* NO LONGER AVAILABLE
        // params: dir, txt sol
        if (command.equals("getSol")) extract.getDatosSol(dir, args[2]);
        // params: dir, periodo, curso
        if (command.equals("getMemos")) extract.getMemos(dir, args[2], args[3]);
        // params: dir, periodo, curso
        if (command.equals("entrega")) extract.getEntregaHonor(dir, args[2], args[3]);
        // params: dir, periodo, curso
        if (command.equals("entregaIO")) extract.getEntregaHonorIO(dir, args[2], args[3]);
        // params: dir, periodo, curso
        if (command.equals("getRespIO")) extract.getRespuestasIO(dir, args[2], args[3]);
        // params: dir, periodo, curso
        if (command.equals("getP")) extract.getP(dir, args[2], args[3]);
        // params: dir, periodo, curso
        if (command.equals("getPIO1")) extract.getPIO1(dir, args[2], args[3]);
        // PECs con errores de campo
        if (command.equals("getnames_error")) extract.getnames_error(dir); 
 */
	
}
