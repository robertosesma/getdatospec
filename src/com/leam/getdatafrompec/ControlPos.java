package com.leam.getdatafrompec;

import java.io.IOException;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.form.PDComboBox;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

public class ControlPos {
	PDField field;
	PDTextField ed;
	PDComboBox co;
	PDRectangle r;

	int page, ncontrols, tipo;
	float y, x;
	double w;
	String name;
	Boolean isW;
	
	// PDF page height
	static final float HEIGHT = 792;
	
	ControlPos(PDField field, int page, PDRectangle r, float w) {
		this.setField(field);
		
		// Coordinates
		this.r = r;
		this.setY(HEIGHT - r.getUpperRightY());
		this.setX(r.getLowerLeftX());
		this.setPage(page);

		this.setW(w);
		this.isW = (w>0);
	}
	
	public void fill()  throws IOException {
		String value = "";

		if (!this.isW()) {
			if (this.tipo == 3) {
				// text field: numeric or memo
				int max = this.ed.getMaxLen();
				if (max>0) {
					// for short (numeric) fields, 1234567890 text according to field length
					// for long (memo) fields, repeated "a" until maximum length
					if (max <= 10) value = "1234567890".substring(0,max);				 
					else if (max <=1000) value = String.join("", Collections.nCopies(max, "a"));	
				}
			}
			
			if (this.tipo == 1 || this.tipo == 2) {
				// combobox field: closed answer
				List<String> opts = this.co.getOptions();
				value = opts.get(opts.size()-1);
			}
					
			if (value.length()>0) this.field.setValue(value);			// set the field value (fill)
		}
	}

	public PDField getField() {
		return this.field;
	}

	public Boolean isOverlapping(float y, int page) {
		Boolean over = ((this.getPage() == page) &&
			(y > this.getY()) && (y <= (this.getY()+this.getHeight())));
		return over;
	}
	
	public void setField(PDField f) {
		this.field = f;
		
		if (f instanceof PDTextField) {
			this.ed = (PDTextField) f;		// text field
			this.tipo = 3;		// tipo = 3 - fixed numeric answer				
		}
		if (f instanceof PDComboBox) {
			this.co = (PDComboBox) f;		// combobox field: closed answer
			List<String> opt = this.co.getOptionsDisplayValues();
			// tipo = 1 - open answer, options are ?,0, ...
			// tipo = 2 - fixed closed answer, options are ?,A,B,.. ?,N,S, ...
			this.tipo = (opt.get(1).equalsIgnoreCase("0") ? 1 : 2); 
		}
	}
	
	public String getValue() {
		String c = "";
		if (this.tipo == 1 || this.tipo == 2) {
			c = this.co.getValue().get(0);
		}
		if (this.tipo == 3) {
			if (this.ed.isMultiline()) {
				c = this.ed.getValue().replace("'", "''");
			} else {
				c = this.ed.getValue().replace(".", ",");				
			}
		}
		return c;
	}

	public String getNumOpc() {
		String c = "null";
		if (this.tipo == 2) {
			c = String.valueOf(this.co.getOptionsDisplayValues().size()-1);
		}
		return c;
	}

	
	public int getTipo() {
		return this.tipo;
	}
	
	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}
	
	public float getY() {
		return this.y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public float getX() {
		return this.x;
	}

	public void setX(float x) {
		this.x = x;
	}
	
	public float getHeight() {
		return this.r.getHeight();
	}
	
	public double getW() {
		return this.w;
	}
	
	public String getWAsString() {
		Formatter f = new Formatter();
		String c = f.format("%.4f",this.w).toString().replace(",", ".");
		f.close();
		return c;
	}

	public void setW(double w) {
		this.w = w;
	}
	
	public Boolean isW() {
		return (this.isW);
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(int i, int iRow, int iCol, String pre) {
		String c = "";
		if (this.isW()) {
			c = "W"+String.format("%02d",i);
		} else {
			c = "P"+String.format("%02d",i) + "_" + pre + Character.toString((char)iRow) + 
					(iCol>0 ? String.format("%d", iCol) : "");
		}
		this.name = c;
	}

	public int getNControls() {
		return this.ncontrols;
	}

	public void setNControls(int n) {
		this.ncontrols = n;
	}
}
